# vi: ft=dockerfile
ARG BASE_IMAGE=r-base:latest
FROM ${BASE_IMAGE}

ARG COMPILE=

RUN test "$COMPILE" = 1 && echo "COMPILE=$COMPILE"

ARG PKG_PREFIX=R-devel
ARG PKG=R-devel.tar.xz
RUN apt-get update
RUN apt-get install -y curl
RUN apt-get install -y xz-utils
RUN curl -L -O "https://cran.r-project.org/src/base-prerelease/$PKG"
RUN tar -xJf $PKG
RUN ls -l
WORKDIR "/$PKG_PREFIX"
RUN cat INSTALL
RUN apt-get install -y gcc
RUN apt-get install -y gfortran
RUN apt-get install -y g++
RUN apt-get install -y libreadline-dev
RUN apt-get install -y zlib1g-dev
RUN apt-get install -y libbz2-dev
RUN apt-get install -y liblzma-dev
RUN apt-get install -y libpcre2-dev
RUN apt-get install -y libcurl4-openssl-dev
RUN apt-get install -y make
RUN apt-get install -y pkg-config
RUN apt-get install -y libcairo2-dev
RUN apt-get install -y libpng-dev
RUN apt-get install -y libjpeg-dev
RUN apt-get install -y libtiff-dev
RUN apt-get install -y libicu-dev
RUN ./configure --disable-long-double --with-x=no
RUN make
RUN make install

RUN apt-get update && apt-get install -y qpdf tidy \
    texlive-base texlive-latex-base texinfo \
    texlive-fonts-recommended texlive-fonts-extra \
    texlive-latex-recommended libssl-dev \
    libcurl4-openssl-dev libxml2-dev pandoc

RUN R --slave --no-restore -e "install.packages(c('chk', 'covr', 'lgr', 'testthat', 'roxygen2', 'rstudioapi', 'R.utils', 'rmarkdown', 'RCurl', 'lintr'), dependencies=TRUE, repos='https://cloud.r-project.org/')"

RUN R --slave --no-restore -e "remotes::install_version('fscache', '>= 1.0.3', upgrade = 'always', dependencies=TRUE, repos='https://cloud.r-project.org/')"
