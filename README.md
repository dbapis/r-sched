# r-sched

[![pipeline](https://gitlab.com/cnrgh/databases/r-sched/badges/release/pipeline.svg?ignore_skipped=true)](https://gitlab.com/cnrgh/databases/r-sched/-/pipelines)
[![coverage](https://gitlab.com/cnrgh/databases/r-sched/badges/release/coverage.svg?ignore_skipped=true)](https://gitlab.com/cnrgh/databases/r-sched/-/pipelines)
[![CRAN](https://www.r-pkg.org/badges/version/sched)](https://cran.r-project.org/package=sched)
[![Total downloads](http://cranlogs.r-pkg.org/badges/grand-total/sched)](https://cran.r-project.org/package=sched)
[![Last month downloads](http://cranlogs.r-pkg.org/badges/last-month/sched)](https://cran.r-project.org/package=sched)

This package offers classes and functions to contact webservers while enforcing
scheduling rules required by the sites.

The `Rule` class allows to define a scheduling rule to respect by specifying
the maximum number of requests for each t seconds, and provides methods to use
that rule.

See [documentation](https://cnrgh.gitlab.io/databases/r-sched/).

## NEWS

### 1.0.3 - 2024-10-01

 * Raise lap time in test, in order to try to avoid failure inside noDL (no double long) CRAN test.

### 1.0.2 - 2024-08-07

 * Do not fail during tests if some URL is not available.

### Version 1.0.1

 * Add trap with `on.exit()` for resetting `options()` when leaving function.
 * Explain SOAP meaning in description.
