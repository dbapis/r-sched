# CRAN noLD test
FROM fedora:36

RUN dnf -y update
RUN dnf -y install gcc xz

# GCC
RUN curl -O "https://ftp.gnu.org/gnu/gcc/gcc-14.2.0/gcc-14.2.0.tar.xz"
RUN tar -xJf "gcc-14.2.0.tar.xz"
WORKDIR "gcc-14.2.0"
RUN dnf -y install g++
RUN dnf -y install gmp-devel
RUN dnf -y install mpfr-devel
RUN dnf -y install libmpc-devel
RUN dnf -y install isl-devel
RUN dnf -y install diffutils
RUN dnf -y install which
RUN dnf -y install file
RUN dnf -y install gettext
#RUN ./contrib/download_prerequisites
RUN ./configure --disable-multilib
RUN make
RUN make install
RUN gcc --version
# libtool --finish /usr/local/libexec/gcc/x86_64-pc-linux-gnu/14.2.0

# R
WORKDIR /
RUN curl -O "https://cran.r-project.org/src/base-prerelease/R-patched_2024-09-18_r87194.tar.xz"
RUN tar -xJf "R-patched_2024-09-18_r87194.tar.xz"
WORKDIR /R-patched
RUN dnf -y install readline-devel
RUN dnf -y install zlib-devel
RUN dnf -y install bzip2-devel
RUN dnf -y install xz-devel
RUN dnf -y install pcre2-devel
RUN dnf -y install libcurl-devel
RUN dnf -y install cairo-devel
RUN dnf -y install libpng-devel
RUN dnf -y install libtiff-devel
RUN dnf -y install libicu-devel
#RUN dnf -y install libjpeg-devel
RUN ./configure --disable-long-double --with-x=no
RUN make
RUN dnf -y install perl
RUN make install

RUN dnf -y install qpdf
RUN dnf -y install tidy
RUN dnf -y install texlive
#RUN dnf -y install texlive-base
#RUN dnf search texlive
#RUN dnf -y install texlive-latex-base
#RUN dnf -y install texinfo
#RUN dnf -y install texlive-fonts-recommended
#RUN dnf -y install texlive-fonts-extra
#RUN dnf -y install texlive-latex-recommended
RUN dnf -y install openssl-devel
#RUN dnf -y install libcurl4-openssl-dev
RUN dnf -y install libxml2-devel
RUN dnf -y install pandoc

RUN R --slave --no-restore -e "install.packages(c('chk', 'covr', 'lgr', 'testthat', 'roxygen2', 'rstudioapi', 'R.utils', 'rmarkdown', 'RCurl', 'lintr'), dependencies=TRUE, repos='https://cloud.r-project.org/')"

RUN R --slave --no-restore -e "remotes::install_version('fscache', '>= 1.0.3', upgrade = 'always', dependencies=TRUE, repos='https://cloud.r-project.org/')"

# Create working folder
ARG dir=/r-sched
WORKDIR $dir

# Copy package files
COPY Makefile DESCRIPTION .Rbuildignore compute_coverage $dir/
COPY R/ $dir/R/
COPY tests/ $dir/tests/
COPY vignettes/ $dir/vignettes/

# Check
ENV HOME=/home/john
RUN mkdir -p "$HOME/.cache/R"
RUN ls -1d "$HOME/.cache"/* "$HOME/.cache/R"/* >fs_state_before.txt 2>/dev/null || true
RUN make lint check cov public
RUN ls -1d "$HOME/.cache"/* "$HOME/.cache/R"/* >fs_state_after.txt 2>/dev/null || true
RUN diff fs_state_before.txt fs_state_after.txt

# Run offline tests
RUN --network=none make test
