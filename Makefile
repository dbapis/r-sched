PACKAGE=$(subst Package: ,,$(shell grep ^Package: DESCRIPTION))
VERSION=$(subst Version: ,,$(shell grep ^Version: DESCRIPTION))
VIGNETTES=$(wildcard vignettes/*.Rmd)
RFLAGS=--slave --no-restore
PKG_FILE=$(PACKAGE)_$(VERSION).tar.gz
R=R $(RFLAGS)
R_VERSIONS=4.2.3 4.3.3 latest

all:

build: NAMESPACE
	$(R) CMD build .

$(PKG_FILE): build

lint:
	$(R) -e "lintr::lint_package()"

check: $(PKG_FILE)
	$(R) CMD check --as-cran "$<"

NAMESPACE: doc

doc:
	$(R) -e "roxygen2::roxygenise()"

install: $(PKG_FILE)
	$(R) CMD INSTALL $(PKG_FILE)

public: check
	mkdir -p "$@"
	for f in sched.Rcheck/sched/doc/*.html ; do sed -e 's!<a href=".*/library/sched/doc/!<a href="!' "$$f" >public/"$$(basename "$$f")" ; done

vignettes: $(VIGNETTES:.Rmd=.html)

%.html: %.Rmd
	R -e 'devtools::build_rmd("$<")'

test:
	$(R) -e "testthat::test_local()" | sed -E 's!([^\d\w])(test_.*\.R)!\1tests/testthat/\2!' | sed -E 's!r-sched/R!R!'

test_win: $(PKG_FILE)
	ftp -a -u ftp://win-builder.r-project.org/R-release/ $(PKG_FILE) # Current R release
	#ftp -a -u ftp://win-builder.r-project.org/R-oldrelease/ $(PKG_FILE) # Previous R release
	#ftp -a -u ftp://win-builder.r-project.org/R-devel/ $(PKG_FILE) # Next R release

test_nold: # CRAN noLD test
	docker build --progress plain -f test_nold.dockerfile -t rsched_test_nold .

dockers: $(addprefix docker_,$(R_VERSIONS))

docker_%:
	docker build --progress plain --build-arg "DOCKER_R_VERSION=$(patsubst docker_%,%,$@)" -t "r-sched:$(patsubst docker_%,%,$@)" .

base_dockers: $(addprefix base_docker_,$(R_VERSIONS))

base_docker_%:
	docker build --progress plain -f base_docker.dockerfile --build-arg "BASE_IMAGE=r-base:$(patsubst base_docker_%,%,$@)" -t "registry.gitlab.com/cnrgh/databases/r-sched:$(patsubst base_docker_%,%,$@)" .

base_docker_devel:
	docker build --progress plain -f base_docker.dockerfile --build-arg "BASE_IMAGE=debian:latest" --build-arg "COMPILE=1" -t "registry.gitlab.com/cnrgh/databases/r-sched:$(patsubst base_docker_%,%,$@)" .

push_base_dockers: $(addprefix push_base_docker_,$(R_VERSIONS))

push_base_docker_%:
	docker push "registry.gitlab.com/cnrgh/databases/r-sched:$(patsubst push_base_docker_%,%,$@)"

cov:
	./compute_coverage

clean:
	$(RM) *.tar.gz
	$(RM) NAMESPACE vignettes/*.html
	$(RM) -r man *.Rcheck
	$(RM) tests/testthat/?.txt tests/testthat/?.tsv tests/testthat/?.csv
	$(RM) -r tests/testthat/wrk
	images="$$(docker images -q r-cache)" ; if test -n "$$images" ; then docker rmi $$images ; fi

.PHONY: all clean check doc install test vignettes lint
