ARG DOCKER_R_VERSION=latest
FROM registry.gitlab.com/cnrgh/databases/r-sched:${DOCKER_R_VERSION}

# Create working folder
ARG dir=/r-sched
WORKDIR $dir

# Copy package files
COPY Makefile DESCRIPTION .Rbuildignore compute_coverage $dir/
COPY R/ $dir/R/
COPY tests/ $dir/tests/
COPY vignettes/ $dir/vignettes/

# Check
ENV HOME=/home/john
RUN mkdir -p "$HOME/.cache/R"
RUN ls -1d "$HOME/.cache"/* "$HOME/.cache/R"/* >fs_state_before.txt 2>/dev/null || true
RUN make lint check cov public
RUN ls -1d "$HOME/.cache"/* "$HOME/.cache/R"/* >fs_state_after.txt 2>/dev/null || true
RUN diff fs_state_before.txt fs_state_after.txt

# Run offline tests
RUN --network=none make test
